module ApplicationHelper

	def avatar_url(user)
		default_url = "#{root_url[0...-1]}#{image_path('default_avatar.png')}"
		gravatar_id = Digest::MD5.hexdigest(user.email.downcase)
		"https://gravatar.com/avatar/#{gravatar_id}.png?s=178&d=#{CGI.escape(default_url)}"
	end

	def formatted_notification(notification)
		%Q{
			#{link_to(notification.actor.username, notification.actor)} #{notification.message}
			 #{link_to('Click here', game_path(notification.target_object))} #{notification.message_action}
		}.html_safe
	end

end
