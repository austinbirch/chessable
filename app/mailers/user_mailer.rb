class UserMailer < ActionMailer::Base
  default from: "hello@chessable.co.uk"
  layout 'email'

  def welcome(user)
  	@user = user
  	mail(:to => user.email, :subject => "Welcome to chessable, #{user.username}")
  end

  def move_made_notification(user, notification)
  	@user = user
  	@notification = notification
  	mail(:to => user.email, :subject => "#{notification.actor.username} #{notification.message}")
  end

  def game_drew_notification(user, notification)
    @user = user
    @notification = notification
    mail(:to => user.email, :subject => "#{notification.actor.username} #{notification.message}")
  end

  def game_won_notification(user, notification)
    @user = user
    @notification = notification
    mail(:to => user.email, :subject => "#{notification.actor.username} #{notification.message}")
  end

  def game_invite_notification(user, game_invite)
    @user = user
    @game_invite = game_invite
    mail(:to => user.email, :subject => "#{game_invite.inviter.username} has invited you to a game!")
  end

  def game_invite_cancelled_notification(user, inviter)
    @user = user
    @inviter = inviter
    mail(:to => user.email, :subject => "#{inviter.username} has cancelled the game invite.")
  end

  def game_invite_accepted_notification(user, game_invite)
    @user = user
    @game_invite = game_invite
    mail(:to => user.email, :subject => "#{game_invite.invited.username} has accepted your game invitation!")
  end
  
  def game_forfeit_notification(user, notification)
    @user = user
    @notification = notification
    mail(:to => user.email, :subject => "#{notification.actor.username} has forfeit the game!")
  end

  def comment_made_notification(user, notification, comment)
    @user = user
    @notification = notification
    @comment = comment
    mail(:to => user.email, :subject => "#{notification.actor.username} has made a comment on your game.")
  end
  
  def subscribed_to_plus(user)
    @user = user
    mail :to => user.email, :subject => 'Subscription to chessable plus'
  end
  
  def payment_received(user)
    @user = user
    mail :to => user.email, :subject => 'Payment received for chessable plus'
  end
  
  def payment_failed(user)
    @user = user
    mail :to => user.email, :subject => 'Payment failed for chessable plus'
  end
  
  def subscription_cancelled(user)
    @user = user
    mail :to => user.email, :subject => 'Subscription cancelled for chessable plus'
  end

  def password_reset(user)
    @user = user
    mail :to => user.email, :subject => 'Password reset instructions'
  end
end
