window.Application ||= {}

class Vector
  constructor: (x = 0, y = 0) ->
    @x = x
    @y = y

class ChessPosition
  constructor: (file = 'a', rank = 0) ->
    @file = file
    @rank = rank
  
  to_s: ->
    "#{@file}#{@rank}"

class Square
  constructor: (position = new Vector, length = 0, color = 'w') ->
    @position = position
    @length = length
    @color = color
    @highlight = false
    @highlight_capture = false

class Piece
  constructor: (color = 'w', type = 'p', position = new Vector()) ->
    @color = color
    @type = type
    @position = position
    @original_position = position
    @scale = 1

class Application.Chessboard
  constructor: (pgn = '', white_enabled = false, black_enabled = false) ->
    @original_pgn = pgn

    #
    # This trick taken from 
    # http://stackoverflow.com/questions/2608022/how-can-i-use-custom-fonts-in-an-html5-canvas-element#2609867
    # 
    # Use it to 'preload' a font-face font ready for use in canvas
    #
    font_img = new Image()
    font_img.src = '/assets/bg_noise.png'

    font_img.onload = =>
      # we'll have click events
      $('canvas#chessboard').bind 'click', @mouse_click
      $('canvas#chessboard').bind 'mousemove', @mouse_move
      $(window).bind 'resize', @resize

      @canvas = $('canvas#chessboard')
      @context = @canvas.get(0).getContext('2d')
      @canvas_length = @canvas.width()
      @square_length = ((Number) @canvas_length) / 8
      @scale_ratio = @square_length

      @white_enabled = white_enabled
      @black_enabled = black_enabled

      if @black_enabled
        @inverted = true
      else
        @inverted = false

      @show_history_array = []

      @board_colors = { 
                        b: '#d08c47', 
                        w: '#fece9e', 
                        highlight: 'rgba(84, 164, 246, 0.7)', 
                        highlight_capture: 'rgba(200, 0, 0, 0.6)',
                        highlight_last_move: 'rgba(100, 50, 200, 0.3)'
                      }

      @piece_colors = { b: '#000000', w: '#f9f9f9' }

      # piece symbols, so that we can use filled in
      # pieces rather than empty ones
      @piece_symbols = {
        p: 'o',
        n: 'j',
        b: 'n',
        r: 't',
        q: 'w',
        k: 'l'
      }

      @squares = []
      @pieces = []
      @game_over_messages = []
      @status_messages = []

      @chess_engine = new Chess()

      @build_squares()
      @update_game()

      # @original_pgn = '1. e4 g5 2. Nf3 Bh6 3. h4 Nf6 4. hxg5 Bxg5 5. Nxg5 h6 6. Nf3 c5 7. b3 Nc6 8. Bb2 e5 9. Nxe5 Rf8 10. Nxc6 bxc6 11. e5 d6 12. exf6 Qe7+ 13. fxe7 Kxe7 14. Rxh6 f6 15. Qe2+ Kd7 16. Rxf6 Rxf6 17. Bxf6 Kc7 18. Nc3 c4 19. g3 Bd7 20. Qe7 Kc8 21. Bd4 Rb8 22. Qxd6 Rb6 23. Na4 Rxb3 24. Bxa7 Be8 25. Bh3+ Kb7 26. Qe7+ Ka8 27. Nb6+ Rxb6 28. Bxb6 Bg6'
      @chess_engine.load_pgn(@original_pgn)

      @update_game()
      @update_information_display()
      
      @render()

  update_game: ->
    @game_over_messages = []
    @status_messages = []

    @check_for_check()
    @check_for_game_end()
    @update_positions()
    @update_last_move()
    @update_information_display()

  update_last_move: ->
    @last_move = @chess_engine.history({ verbose: true })[(@chess_engine.history().length-1)]

  update_information_display: ->
    # display the pgn
    $('#move_history').html(@chess_engine.pgn({ newline_char: '<br>' }))

    # display game status
    if @status_messages.length > 0
      $('#game_status').html('')
      $('#game_status').append('<p class="header">Status:</p>')
      for message in @status_messages
        $('#game_status').append("<span>#{message}</span><br>")
    else
      $('#game_status').html('')

    # display game over messages
    if @game_over_messages.length > 0
      $('#game_over_status').html('')
      for message in @game_over_messages
        $('#game_over_status').append("<p>#{message}</p>")
    else
      $('#game_over_status').html('')
    

  check_for_check: ->
    if @chess_engine.in_check()
      @status_messages.push 'Check.'

  check_for_game_end: ->
    if @chess_engine.in_checkmate()
      @game_over_messages.push 'Checkmate.'
    
    if @chess_engine.in_draw()
      @game_over_messages.push 'Draw.'
    
    if @chess_engine.in_stalemate()
      @game_over_messages.push 'Stalemate.'
    
    if @chess_engine.in_threefold_repetition()
      @game_over_messages.push 'Threefold repetition.'
    
    if @chess_engine.game_over() != false
      @game_over_messages.push 'Game over.'
  
  show_previous: ->
    @show_history_array.push @chess_engine.undo()
    @update_game()
    @render()
  
  show_beginning: ->
    while @chess_engine.history().length != 0
      @show_history_array.push @chess_engine.undo()
    @update_game()
    @render()

  show_next: ->
    @chess_engine.move(@show_history_array.pop())
    @update_game()
    @render()

  show_half_move: (number = 0) ->
    if (number - 1) < (@chess_engine.history().length + @show_history_array.length)
      # reset to the latest move first
      @show_reset()

      while @chess_engine.history().length != number
        @show_history_array.push @chess_engine.undo()
      @update_game()
      @render()

  show_reset: ->
    @chess_engine.load_pgn(@original_pgn)
    @show_history_array = []
    @update_game()
    @render()

  get_display_half_move_number: ->
    @chess_engine.history().length

  update_positions: ->
    @pieces = []
    for y in [0..7]
      for x in [0..7]
        pos = @ui_to_chess(new Vector(x,y)).to_s()
        chess_piece = @chess_engine.get(pos)
        if chess_piece?
          @pieces.push new Piece(chess_piece.color, chess_piece.type, new Vector(x,y))
  
  build_squares: ->
    black = false
    for x in [0..7]
      # swap black
      if black == true
        black = false
      else
        black = true

      for y in [0..7]
        # swap black
        if black == true
          black = false
        else
          black = true

        pos = new Vector(x, y)
        length = @square_length
        if black
          color = 'b'
        else
          color = 'w'
        @squares.push new Square(pos, length, color)
  
  clear_all_highlights: ->
    for square in @squares
      square.highlight_capture = false
      square.highlight = false
    
  render: ->
    context = @context
    context.canvas.height = @canvas_length
    context.canvas.width = @canvas_length

    if @inverted
      context.scale(1, -1)
      context.translate(0, -@canvas_length)

    # clear the screen
    context.clearRect(0, 0, @canvas_length, @canvas_length)

    # draw the board
    for square in @squares
      context.fillStyle = @board_colors[square.color]

      if @inverted
        context.save()
        context.scale(-1, 1)
        context.translate(-@canvas_length, 0)
        
      context.fillRect(square.position.x * @scale_ratio,
                       square.position.y * @scale_ratio,
                       square.length, square.length)

      if square.highlight
        context.fillStyle = @board_colors['highlight']
        context.fillRect(square.position.x * @scale_ratio,
                       square.position.y * @scale_ratio,
                       square.length, square.length)

      if square.highlight_capture
        context.fillStyle = @board_colors['highlight_capture']
        context.fillRect(square.position.x * @scale_ratio,
                       square.position.y * @scale_ratio,
                       square.length, square.length)
      if @inverted
        context.restore()

    if @last_move
      chess_pos_from = new ChessPosition(@last_move.from[0], @last_move.from[1])
      square = @square_at_position(@chess_to_ui(chess_pos_from))
      unless square.highlight || square.highlight_capture
        context.fillStyle = @board_colors['highlight_last_move']

        if @inverted
          context.save()
          context.scale(-1, 1)
          context.translate(-@canvas_length, 0)

        context.fillRect(square.position.x * @scale_ratio,
                       square.position.y * @scale_ratio,
                       @square_length, @square_length)
        if @inverted
          context.restore()

      chess_pos_to = new ChessPosition(@last_move.to[0], @last_move.to[1])
      square = @square_at_position(@chess_to_ui(chess_pos_to))
      unless square.highlight || square.highlight_capture
        context.fillStyle = @board_colors['highlight_last_move']

        if @inverted
          context.save()
          context.scale(-1, 1)
          context.translate(-@canvas_length, 0)

        context.fillRect(square.position.x * @scale_ratio,
                       square.position.y * @scale_ratio,
                       @square_length, @square_length)
        if @inverted
          context.restore()
    
    # draw the pieces
    for piece in @pieces
      context.fillStyle = @piece_colors[piece.color]
      context.font = "#{Math.floor(@scale_ratio * 0.6 * piece.scale)}px chess-font"
      offset = 0

      if @inverted
        context.save()

        context.scale(-1, 1)
        context.translate(-@canvas_length, 0)

        context.translate((piece.position.x * @scale_ratio), (piece.position.y * @scale_ratio))
        context.rotate(180 * Math.PI/180)
        context.translate(-(piece.position.x * @scale_ratio), -(piece.position.y * @scale_ratio))
        offset = @square_length

      context.fillText(@piece_symbols[piece.type],
                       (piece.position.x * @scale_ratio) + (@scale_ratio * 0.2) - offset,
                       (piece.position.y * @scale_ratio) + (@scale_ratio * 0.65) - offset)

      if @inverted
        context.restore()


    # update button states
    @update_button_states()
  
  update_button_states: ->
    # reset all to visible, disable those that can't be used
    $('input[type="submit"]#confirm_move, #undo_button, #show_next, #show_reset, #show_previous, #show_beginning').removeAttr('disabled')
    $('input[type="submit"]#confirm_move, #undo_button, #show_next, #show_reset, #show_previous, #show_beginning').removeClass('disabled')

    if @show_history_array.length > 0
      # disable the submit and undo buttons
      $('input[type="submit"]#confirm_move, #undo_button').attr('disabled', 'disabled')
      $('input[type="submit"]#confirm_move, #undo_button').addClass('disabled')
    
    if @show_history_array.length == 0
      $('#show_next, #show_reset').attr('disabled', 'disabled')
      $('#show_next, #show_reset').addClass('disabled')
    
    if @chess_engine.history().length == 0
      $('#show_previous').attr('disabled', 'disabled')
      $('#show_previous').addClass('disabled')
      $('#show_beginning').attr('disabled', 'disabled')
      $('#show_beginning').addClass('disabled')
    
    if @chess_engine.pgn() == @original_pgn
      $('#undo_button, input[type="submit"]#confirm_move').attr('disabled', 'disabled')
      $('#undo_button, input[type="submit"]#confirm_move').addClass('disabled')

  # coordinate/chess position conversion
  ui_to_chess: (position) ->
    files = ['a','b','c','d','e','f','g','h']
    rank = (position.y - 8) * -1
    file = files[position.x]
    new ChessPosition(file, rank)
  chess_to_ui: (chess_position) ->
    files = ['a','b','c','d','e','f','g','h']
    row = (chess_position.rank - 8) * -1
    col = files.indexOf(chess_position.file)
    new Vector(col, row)
  coords_to_position: (coords) ->
    x_coord = Math.floor(coords.x / @scale_ratio)
    y_coord = Math.floor(coords.y / @scale_ratio)
    new Vector(x_coord, y_coord)

  
  # piece location
  piece_at_position: (position) ->
    found_piece = null
    for piece in @pieces
      if piece?
        if position.x == piece.position.x &&
          position.y == piece.position.y
           found_piece = piece
    found_piece

  # square location
  square_at_position: (position) ->
    found_square = null
    for square in @squares
      if square?
        if position.x == square.position.x &&
          position.y == square.position.y
            found_square = square
    found_square
  
  undo: ->
    @chess_engine.load_pgn(@original_pgn)
    @update_game()
    @render()


  mouse_click: (event) =>
    if (event.pageX || event.pageY)
      x = event.pageX
      y = event.pageY
   
      # adjust for canvas offset
      x -= $(@canvas).offset().left
      y -= $(@canvas).offset().top

      if @inverted
        y = @canvas_length - y
        x = @canvas_length - x

      click_coords = new Vector(x, y)
      click_pos = @coords_to_position(click_coords)

      unless @chess_engine.game_over() || @show_history_array.length > 0
        if @selected_piece?
          # we want to put it down, calculate the move,
          # and send it to the chess engine
          #
          # remember to check to see whether it was put
          # back in the same place
          if click_pos == @selected_piece.original_position
            @render()
            return
          else
            # 'put it down'
            from_chess_position = @ui_to_chess(@selected_piece.original_position).to_s()
            to_chess_position = @ui_to_chess(click_pos).to_s()

            to_chess = @ui_to_chess(click_pos)
            promotion_flag = ''
            if @selected_piece.type == 'p' && (to_chess.rank == 8 || to_chess.rank == 1)
              promotion_flag = 'q'

            @chess_engine.move({ from: from_chess_position, to: to_chess_position, piece: @selected_piece.type, promotion: promotion_flag })
            @update_game()
            @selected_piece = null
            @clear_all_highlights()
        else  
          # we are picking a piece up
          piece = @piece_at_position(click_pos)
          if piece?

            if (@black_enabled == true && piece.color == 'b') || (@white_enabled == true && piece.color == 'w')
              avail_moves = @chess_engine.moves({ verbose: true })
              for move in avail_moves
                if move.from == @ui_to_chess(click_pos).to_s()
                  square = @square_at_position(@chess_to_ui(new ChessPosition(move.to[0], move.to[1])))
                  if square?
                      if move.flags.match(/[ec]/)
                        # highlight for a capture
                        square.highlight_capture = true
                      else
                        # highlight for available
                        square.highlight = true

              @selected_piece = piece
              @selected_piece.scale = '1.2'
          
        @render()


  mouse_move: (event) =>
    if (event.pageX || event.pageY)
      x = event.pageX
      y = event.pageY
   
      # adjust for canvas offset
      x -= $(@canvas).offset().left
      y -= $(@canvas).offset().top

      if @inverted
        y = @canvas_length - y
        x = @canvas_length - x

      click_coords = new Vector(x, y)

      if @selected_piece?
        x = (x / @scale_ratio) - (@scale_ratio / (2 * @scale_ratio))
        y = (y / @scale_ratio) - (@scale_ratio / (2 * @scale_ratio))
        @selected_piece.position = new Vector(x, y)
      
      @render()

  resize: (event) =>
    @canvas_length = @canvas.width()
    @square_length = ((Number) @canvas_length) / 8
    @scale_ratio = @square_length
    @render()
