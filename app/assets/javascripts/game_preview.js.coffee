window.Application ||= {}

class Application.GamePreview
  constructor: (canvas, pgn = null, fen = null, inverted = false) ->
    @canvas = $(canvas)
    @canvas_length = @canvas.width()
    @square_length = ((Number) @canvas_length) / 8
    @context = @canvas.get(0).getContext('2d')
    @inverted = inverted

    @board_colors = { 
                      b: '#d08c47',
                      w: '#fece9e',
                    }
    
    @piece_symbols = {
      p: 'o',
      n: 'j',
      b: 'n',
      r: 't',
      q: 'w',
      k: 'l'
    }
     
    @chess_engine = new Chess()

    #start_time = new Date().getTime()
    if fen
      #console.log 'FEN start: ' + start_time
      @chess_engine.load(fen)
    else
      #console.log 'PGN start: ' + start_time
      @chess_engine.load_pgn(pgn)

    #end_time = new Date().getTime()
    #console.log 'end time: ' + end_time
    #console.log 'diff = ' + (end_time - start_time)
    
    @render()
  
  render: ->
    context = @context
    context.canvas.height = @canvas_length
    context.canvas.width = @canvas_length

    if @inverted
      context.save()
      context.scale(1, -1)
      context.translate(0, -@canvas_length)
    
    # draw the board
    black = false
    for x in [0..7]
      if black == true
        black = false
      else
        black = true
      
      for y in [0..7]
        if black == true
          black = false
        else
          black = true
        
        if black
          context.fillStyle = @board_colors['b']
        else
          context.fillStyle = @board_colors['w']
        
        context.fillRect(@square_length * x, @square_length * y, @square_length, @square_length)

    if @inverted
      context.restore()
        
    # draw the pieces
    context.font = "#{Math.floor(@square_length * 0.6)}px chess-font"
    for x in [0..7]
      for y in [0..7]
        files = ['a','b','c','d','e','f','g','h']
        file = files[x]
        rank = (y - 8) * -1
        piece = @chess_engine.get("#{file}#{rank}")
        if piece?
          if piece.color == 'b'
            context.fillStyle = 'rgba(0, 0, 0, 255)'
          else
            context.fillStyle = 'rgba(255, 255, 255, 255)'
            
          offset = 0
          if @inverted
            context.save()
            context.scale(1, -1)
            context.translate(0, -@canvas_length)

            context.scale(-1, 1)
            context.translate(-@canvas_length, 0)

            context.translate((x * @square_length), (y * @square_length))
            context.rotate(Math.PI)
            context.translate(-(x * @square_length), -(y * @square_length))
            offset = @square_length

          context.fillText(@piece_symbols[piece.type],
                           (@square_length * x) + (@square_length * 0.2) - offset,
                           (@square_length * y) + (@square_length * 0.65) - offset)

          if @inverted
            context.restore()
