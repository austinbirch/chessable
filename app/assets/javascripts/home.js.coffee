# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
  if $('#home_controller').length > 0

    $('.tweet').tweet
      username: 'chessable'
      join_text: 'auto'
      avatar_size: 32
      count: 4
      auto_join_text_default: "we said:"
      auto_join_text_ed: "we:"
      auto_join_text_ing: "we were:"
      auto_join_text_reply: "we replied to:"
      auto_join_text_url: "we said:"
      loading_text: 'loading tweets...'


    $('.tweet').bind 'loaded', ->
      $('.tweet_odd, .tweet_even').bind 'click', ->
        link_url = $(this).children('span.tweet_time').children('a').attr('href')
        window.location.href = link_url

    $('#read_more a').click (event) ->
      $('html, body').animate
        scrollTop: $('#about').offset().top
        'slow'
