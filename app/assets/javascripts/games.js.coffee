# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  if $('#games_controller').length > 0

    $('#move_form').bind 'submit', (event) ->
      $('#move_form input[type="hidden"]#game_last_move').val(Application.chess_instance.chess_engine.history().pop())

    $('#game_form_container #undo_button').bind 'click', (event) ->
      unless $(@).hasClass('disabled')
        Application.chess_instance.undo()
      event.preventDefault()
    
    $('#game_wrapper #show_previous').bind 'click', (event) ->
      unless $(@).hasClass('disabled')
        Application.chess_instance.show_previous()
      event.preventDefault()

    $('#game_wrapper #show_next').bind 'click', (event) ->
      unless $(@).hasClass('disabled')
        Application.chess_instance.show_next()
      event.preventDefault()

    $('#game_wrapper #show_reset').bind 'click', (event) ->
      unless $(@).hasClass('disabled')
        Application.chess_instance.show_reset()
      event.preventDefault()
    
    $('#game_wrapper #show_beginning').bind 'click', (event) ->
      unless $(@).hasClass('disabled')
        Application.chess_instance.show_beginning()
      event.preventDefault()


    # auto-scroll the chat history on page load
    $('#history').scrollTop($('#history')[0].scrollHeight)

    $('#new_comment').bind 'submit', (event) ->
      $('#new_comment input[type="hidden"]#comment_half_move_number').val(Application.chess_instance.get_display_half_move_number())

    # set up click handling for comments linked to chessboard display
    $('div.comment.linked').each ->
      $(this).hover ->
        $(this).css('cursor', 'pointer')

      $(this).click ->
        class_list = $(this).attr('class').split(/\s+/)
        for item in class_list
          if item.match(/target_/)
            number = item.replace(/target_/, '')
            Application.chess_instance.show_half_move(parseInt(number))
            $('html, body').animate
              scrollTop: $('#game_wrapper').offset().top
              'slow'

            original_bg_image = $('#game_wrapper').css('background-image')
            $('#game_wrapper').css('background-image', 'none')
            original_bg = $(this).css('background-color')
            $('#game_wrapper').css('background-color', '#676784')
            $('#game_wrapper').animate
              backgroundColor: original_bg
              'slow'
              ->
                $('#game_wrapper').css('background-image', original_bg_image)
