class HomeController < ApplicationController
  
  def index
  end

  def forbidden
  end
  
  def terms
  end
  
  def privacy
  end
  
  def more_info
  end
  
end
