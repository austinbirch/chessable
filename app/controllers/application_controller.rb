class ApplicationController < ActionController::Base
  protect_from_forgery

  private
  def current_user
  	@current_user ||= User.where(auth_token: cookies[:auth_token]).first if cookies[:auth_token]
  end

  def authenticate_user!
  	if current_user.nil?
  		redirect_to new_session_path, :notice => 'You need to be logged in to do that.'
  	end
  end

  helper_method :current_user
end
