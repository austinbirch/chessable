class UsersController < ApplicationController
  before_filter :authenticate_user!, :only => [:edit, :update]
  before_filter :authorize_user, :only => [:edit, :update]

  def random
    user = current_user

    until user != current_user
      user = User.skip(rand(User.count)).limit(1).first
    end

    redirect_to user_path(user)
  end

  def index
    if params[:search]
      @users = User.all.where(:username => /#{Regexp.escape(params[:search])}/i).to_a
    else    
      @users = User.all.to_a
      # @users.delete current_user if current_user
    
      case params[:sort_by]
      when /username/
        @users.sort_by! { |user| user.username.downcase }
        @sorted_by = 'username'
      when /most_wins/
        @users.sort! { |a, b| a.wins <=> b.wins }.reverse!
        @sorted_by = 'wins'
      when /most_losses/
        @users.sort! { |a, b| a.losses <=> b.losses }.reverse!
        @sorted_by = 'losses'
      when /most_draws/
        @users.sort! { |a, b| a.draws <=> b.draws }.reverse!
        @sorted_by = 'draws'
      else
        @users.sort! { |a, b| a.wins <=> b.wins }.reverse!
        @sorted_by = 'wins'
      end
    end
  end

  def show
    @user = User.find_by_slug(params[:id])
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(params[:user])
    @user.username.strip!
    if @user.save
      redirect_to root_path, :notice => 'A verification email has been sent. Verify your account by clicking the link within the email.'
    else
      render 'new'
    end
  end

  def update
    @user.password = params[:user][:password]
    @user.password_confirmation = params[:user][:password_confirmation]

    @user.receive_email_notifications = params[:user][:receive_email_notifications]

    if @user.save!
      redirect_to edit_user_path(@user), :notice => 'Account settings updated.'
    else
      render 'edit'
    end
  end

  private

  def authorize_user
    @user = User.find_by_slug(params[:id])
    redirect_to forbidden_path unless @user == current_user
  end

end
