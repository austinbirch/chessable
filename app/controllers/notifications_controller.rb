class NotificationsController < ApplicationController
  before_filter :authenticate_user!, :check_current_user

  def update
    @user = current_user
    @notification = @user.notifications.find(params[:id])

    if @notification.update_attributes(params[:notification])
      redirect_to :back
    else
      redirect_to :back, :notice => 'Something went wrong...'
    end

  end

  private

  def check_current_user
    redirect_to forbidden_path unless current_user == User.find_by_slug(params[:user_id])
  end

end
