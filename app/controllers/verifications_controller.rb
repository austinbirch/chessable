class VerificationsController < ApplicationController

	def index
		@user = User.where(:verification_token => params[:verification_token]).first

		if @user && !@user.verified
			@user.verified = true
			@user.save

			# log them in
			cookies[:auth_token] = @user.auth_token
			redirect_to new_user_subscription_path(@user), :notice => 'Email verified!'
		else
			redirect_to root_path, :alert => 'Verification token incorrect'
		end

	end

end
