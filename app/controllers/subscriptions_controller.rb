class SubscriptionsController < ApplicationController
  before_filter :authenticate_user!, :check_and_get_user

  def new
    @subscription = @user.subscriptions.build
  end

  private

  def check_and_get_user
    @user = User.find_by_slug(params[:user_id])
    redirect_to forbidden_path unless @user == current_user
  end

end
