class CommentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

  def create
    @comment = @game.comments.build

    @comment.user = current_user
    @comment.content = params[:comment][:content]
    @comment.half_move_number = params[:comment][:half_move_number]

    respond_to do |format|
      if @comment.save
        
        target_player = @game.white_player == current_user ? @game.black_player : @game.white_player
        notification = target_player.notifications.create(:actor => current_user,
                                                          :message => 'made a comment on your game.',
                                                          :message_action => 'to view it.',
                                                          :target_object => @game)
        if target_player.receive_email_notifications
          UserMailer.comment_made_notification(target_player, notification, @comment).deliver
        end
         
        format.html { redirect_to game_path(@game), :notice => 'Comment added successfully.' }
        format.js
      else
        format.html { redirect_to game_path(@game), :notice => 'Something went wrong...' }
        format.js
      end
    end
  end

  private

  def authorize_user
    @user = current_user
    @game = Game.find(params[:game_id])
    redirect_to forbidden_path unless @user == @game.white_player || @user == @game.black_player
  end

end
