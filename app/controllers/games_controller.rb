class GamesController < ApplicationController

  def index
    if params[:playername]
      @user = User.where(:username => /#{Regexp.escape(params[:playername])}/i).first

      if @user
        @games = Game.any_of({:black_player_id => @user.id}, {:white_player_id => @user.id} )

        if params[:result]
          @games = @games.where(:game_over => true)

          if params[:result] == 'won'
            @result = 'won'
            @games = @games.where(:winner_id => @user.id).to_a
          elsif params[:result] == 'lost'
            @result = 'lost'
            @games = @games.excludes(:winner_id => @user.id).to_a
          elsif params[:result] == 'draw'
            @result = 'drawn'
            @games = @games.where(:in_draw => true).to_a
          end
        end

        @games = @games.to_a
      end
    end

    @active_games = Game.all.where(:active => true)
    @inactive_games = Game.all.where(:active => false)
  end

  def create
    @game = Game.new
    @game.black_player = User.find_by_slug(params[:game][:black_player])
    @game.white_player = current_user
    
    if @game.save!
      redirect_to game_path(@game)
    else
      redirect_to :back, :alert => 'Something went wrong... : ('
    end
  end

  def update
    @game = Game.find(params[:id])

    if @game.next_turn == 'w' && current_user == @game.white_player || 
      @game.next_turn == 'b' && current_user == @game.black_player
      
      @game.last_move = params[:game][:last_move]
    
      if params[:commit] =~ /forfeit/i
        if @game.next_turn == 'w'
          @game.forfeit_player_id = @game.white_player.id
        else
          @game.forfeit_player_id = @game.black_player.id
        end
        @game.last_move = nil
      end
      
      if @game.save!
        redirect_to game_path(@game), :notice => 'Move sent successfully.'
      else
        redirect_to game_path(@game), :alert => 'Something went wrong...'
      end
    else
      redirect_to :back, :alert => 'It is not your turn!'
    end
  end

  def show
    @game = Game.find(params[:id])
  end

end
