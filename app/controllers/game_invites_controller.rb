class GameInvitesController < ApplicationController
	before_filter :authenticate_user!
	before_filter :get_game_invite, :only => [:show, :update, :destroy]
	before_filter :check_invited_player, :only => [:update]

	def show
		if current_user == @game_invite.invited || current_user == @game_invite.inviter
		else
			redirect_to forbidden_path
		end
	end

	def create
		@invited = User.find_by_slug(params[:game_invite][:invited])
		@game_invite = GameInvite.new(:inviter => current_user, :invited => @invited)
		if @game_invite.save
			redirect_to :back, :notice => 'Game invite sent.'
		else
      if @game_invite.errors[:inviter].to_s =~ /plus/
        alert = 'You need a plus account to be able to play another game at this time.'
      else
        alert = 'Something went wrong...'
      end
			redirect_to :back, :alert => alert
		end
	end

	def update
		@game_invite.invite_status = params[:game_invite][:invite_status]

		if @game_invite.save
			if @game_invite.invite_status == GameInvite::INVITE_STATUS[:accepted]
				redirect_to game_path(@game_invite.game)
			else
				redirect_to user_dashboard_path(current_user), :notice => 'Game invite rejected.'
			end
		else
			render 'show'
    end
	end

  def destroy
    if @inviter == current_user
      if @game_invite.destroy
        redirect_to user_dashboard_path(current_user)
        if @invited.receive_email_notifications
          UserMailer.game_invite_cancelled_notification(@invited, @inviter).deliver
        end
      else
        redirect_to :back, :alert => 'Something went wrong...'
      end
    else
      redirect_to forbidden_path
    end
  end

	private

	def get_game_invite
		@game_invite = GameInvite.find(params[:id])
    @invited = @game_invite.invited
    @inviter = @game_invite.inviter
	end

	def check_invited_player
		game_invite = GameInvite.find(params[:id])
		redirect_to forbidden_path unless current_user == game_invite.invited
	end

end
