class PaymentNotificationsController < ApplicationController

  def create
    PaymentNotification.create!(:params => params,
                                :user_id => params[:custom],
                                :status => params[:payment_status],
                                :transaction_id => params[:txn_id],
                                :transaction_type => params[:txn_type])
    render :nothing => true
  end

end
