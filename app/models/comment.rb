class Comment
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :user
  belongs_to :commentable, :polymorphic => true


  field :content 
  field :half_move_number

  before_validation :strip_whitespace
  validates :content,
            :length => { :minimum => 2 },
            :allow_nil => false

  private

  def strip_whitespace
    self.content = self.content.strip unless self.content.nil?
  end

end
