class GameInvite
  include Mongoid::Document
  include Mongoid::Timestamps

  INVITE_STATUS = { :accepted => 'accepted', :rejected => 'rejected', :pending => 'pending' }

  before_update :accept_or_reject_game
  after_create :send_invite_notification
  after_update :send_notification_if_accepted

  belongs_to :inviter, :class_name => 'User'
  belongs_to :invited, :class_name => 'User'
  belongs_to :game

  validates :invited, :presence => true
  validates :inviter, :presence => true
  validate :not_inviting_self

  field :invite_status, :default => INVITE_STATUS[:pending]

  validate :invited_meets_subscription_requirements,
           :on => :update,
           :if => lambda { |game_invite| game_invite.invite_status == INVITE_STATUS[:accepted] }

  validate :inviter_meets_subscription_requirements,
           :on => :create,
           :unless => lambda { |game_invite| game_invite.inviter.nil? }

  private

  def not_inviting_self
  	errors.add(:invited, "cannot be yourself!") if inviter == invited
  end

  def accept_or_reject_game
  	if invite_status == INVITE_STATUS[:accepted]
  		self.game = Game.new
      self.game.white_player = invited
      self.game.black_player = inviter
      self.game.save
      self.destroy
  	elsif invite_status == INVITE_STATUS[:rejected]
  		self.destroy
  	end
  end

  def send_invite_notification
    UserMailer.game_invite_notification(invited, self).deliver if invited.receive_email_notifications
  end

  def send_notification_if_accepted
    UserMailer.game_invite_accepted_notification(inviter, self).deliver if invite_status == INVITE_STATUS[:accepted] && inviter.receive_email_notifications
  end

  def invited_meets_subscription_requirements
    invited_active_games = invited.games.select { |game| game.active == true }
    if invited.current_subscription.nil? && invited_active_games.count >= 2
      errors.add :invited, "needs a plus subscription to be able to play another game at this time."
      return false
    else
      return true
    end
  end

  def inviter_meets_subscription_requirements
    inviter_active_games = inviter.games.select { |game| game.active == true }
    if inviter.current_subscription.nil? && inviter_active_games.count >= 2
      errors.add :inviter, "needs a plus subscription to be able to play another game at this time."
      return false
    else
      return true
    end
  end

end
