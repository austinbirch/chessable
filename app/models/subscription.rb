class Subscription
  include Mongoid::Document
  include Mongoid::Timestamps
  
  belongs_to :user
  before_update :check_subscription_status

  field :start_date
  field :end_date
  field :paid, :type => Boolean, :default => false
  field :current
  field :cancelled

  def encrypted_paypal(return_url, notify_url)
    values = {
      :cert_id => APP_CONFIG[:paypal_cert_id],
      :cmd => '_xclick-subscriptions',
      :business => APP_CONFIG[:paypal_email],
      :return => return_url,
      :rm => '1',
      :notify_url => notify_url,
      :currency_code => 'GBP',
      :no_note => '1',
      :src => '1',
      :no_shipping => '1',
      :reattempt => '1',
      :custom => self.user.id,
      :a3 => '3.00',
      :p3 => '1',
      :t3 => 'M',
      :item_name => 'chessable - plus'
    }
    encrypt_for_paypal(values)
  end

  PAYPAL_CERT_PEM = File.read("#{Rails.root}/paypal_certs/#{APP_CONFIG[:paypal_cert]}")
  APP_CERT_PEM = File.read("#{Rails.root}/paypal_certs/app_cert.pem")
  APP_KEY_PEM = File.read("#{Rails.root}/paypal_certs/app_key.pem")

  def encrypt_for_paypal(values)
    signed = OpenSSL::PKCS7::sign(OpenSSL::X509::Certificate.new(APP_CERT_PEM), OpenSSL::PKey::RSA.new(APP_KEY_PEM, ''), values.map { |k, v| "#{k}=#{v}" }.join("\n"), [], OpenSSL::PKCS7::BINARY)  
    OpenSSL::PKCS7::encrypt([OpenSSL::X509::Certificate.new(PAYPAL_CERT_PEM)], signed.to_der, OpenSSL::Cipher::Cipher::new("DES3"), OpenSSL::PKCS7::BINARY).to_s.gsub("\n", "")  
  end
  
  private
  
  def check_subscription_status
    if self.cancelled
      self.current = false
      return true
    end
  end
  
end
