class Game
  include ActionView::Helpers::JavaScriptHelper
  include Mongoid::Document
  include Mongoid::Timestamps

  attr_accessible :last_move

  before_update :set_game_status, :check_for_win, :reset_last_move
  before_create :set_default_pgn 
  before_validation :check_forfeit

  has_many :comments, :as => :commentable

  belongs_to :white_player, :class_name => 'User', :inverse_of => :white_games
  belongs_to :black_player, :class_name => 'User', :inverse_of => :black_games
  belongs_to :winner, :class_name => 'User'

  validates :white_player, :presence => true
  validates :black_player, :presence => true

  validate :last_move_must_be_valid_chess_move, :on => :update, :unless => lambda { last_move.nil? }

  field :last_move
  field :next_turn, :default => 'w'
  field :pgn
  field :fen
  field :active, :type => Boolean, :default => true
  field :forfeit_player_id

  field :in_check, :type => Boolean, :default => false
  field :in_checkmate, :type => Boolean, :default => false
  field :in_stalemate, :type => Boolean, :default => false
  field :in_draw, :type => Boolean, :default => false
  field :game_over, :type => Boolean, :default => false

  def friendly_name
    "#{white_player.username}-vs-#{black_player.username}"
  end

  def last_move_must_be_valid_chess_move
    unless last_move.nil?
      source = File.read(Rails.root.join('lib/chess_validator.js'))
      context = ExecJS.compile(source)
      output = context.call('is_valid_move', pgn, last_move)

      if output.nil?
        errors.add :last_move, 'is invalid.'
      end
    end
  end
  
  def next_player
    if next_turn == 'w'
      return white_player
    elsif next_turn == 'b'
      return black_player
    end
  end

  private
  
  def check_forfeit
    unless forfeit_player_id.nil?
      self.last_move = nil
    end
  end
  
  def set_default_pgn
    self.pgn = "[White \"#{self.white_player.to_param}\"]\n[Black \"#{self.black_player.to_param}\"]\n"
  end

  def set_game_status
    Rails.logger.info "GET GAME STATUS"
    
    source = File.read(Rails.root.join('lib/chess_validator.js'))
    context = ExecJS.compile(source)  
    
    if pgn.nil?
      self.pgn = "[White \"#{self.white_player.to_param}\"]\n[Black \"#{self.black_player.to_param}\"]\n"
    else
      self.pgn = context.call('pgn_after_move', pgn, last_move)
    end
    
    ret_hash = context.call('validate', self.pgn)
    self.next_turn = ret_hash['next_turn']
    self.fen = ret_hash['fen']
    self.in_check = ret_hash['in_check']
    self.in_checkmate = ret_hash['in_checkmate']
    self.in_stalemate = ret_hash['in_stalemate']
    self.in_draw = ret_hash['in_draw']
    self.game_over = ret_hash['game_over']
    
    Rails.logger.info "GOT GAME STATUS"
  end

  def reset_last_move
    self.last_move = nil
  end

  def check_for_win
    if in_checkmate || !forfeit_player_id.nil?
      self.active = false
      self.game_over = true      
      if next_turn == 'w'
        self.winner = black_player
      elsif next_turn == 'b'
        self.winner = white_player
      end      
    end
  end

end
