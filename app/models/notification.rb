class Notification
  include Mongoid::Document
  include Mongoid::Timestamps
  
  after_update :destroy_if_read

  belongs_to :user
  belongs_to :target_object, :class_name => 'Game'
  belongs_to :actor, :class_name => 'User'

  field :message
  field :message_action
  field :read, :type => Boolean, :default => false
  
  private
  
  def destroy_if_read
    if read
      self.destroy
      true
    end
  end

end
