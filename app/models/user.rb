class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword
  include Mongoid::Slug
  
  has_secure_password

  before_create :generate_verification_token
  after_create :send_welcome_email

  has_many :white_games, :class_name => 'Game', :inverse_of => :white_player
  has_many :black_games, :class_name => 'Game', :inverse_of => :black_player
  has_many :notifications
  has_many :subscriptions
  has_many :payment_notifications

  field :email
  field :username
  field :password_digest
  field :receive_email_notifications, :type => Boolean, :default => true
  field :auth_token
  field :password_reset_token
  field :password_reset_sent_at
  field :verification_token
  field :verified, :type => Boolean, :default => false

  field :wins_as_white, :default => 0
  field :wins_as_black, :default => 0
  field :losses_as_white, :default => 0
  field :losses_as_black, :default => 0
  field :draws_as_white, :default => 0
  field :draws_as_black, :default => 0
  
  slug :username

  attr_accessible :email, :username, :password, :password_confirmation
  attr_accessor :wins, :draws, :losses

  # filters
  before_create { generate_token(:auth_token) }

  # validations
  validates :password, 
            :presence => true,
            :length => { :minimum => 8 },
            :on => :create

  validates :password,
            :allow_blank => true,
            :length => { :minimum => 8 },
            :confirmation => true,
            :on => :update


  validates :email, :presence => true, :uniqueness => true, :email_format => true
  validates :username,
            :presence => true,
            :uniqueness => true,
            :format => { :with => /[A-Za-z0-9]+/ },
            :length => { :minimum => 2, :maximum => 24 }

  def games
    (white_games + black_games).sort_by { |game| game.updated_at }.reverse!
  end
  
  def active_games
    games.select { |game| game.active }
  end
  
  def won_games
    games.select { |game| self == game.winner }
  end
  
  def lost_games
    games.select { |game| self != game.winner && !game.active }
  end
  
  def drew_games
    games.select { |game| game.in_draw }    
  end

  def unread_notifications
    notifications.where(:read => false)
  end

  def current_subscription
    subscriptions.where(:current => true).first
  end

  def pending_game_invites
    GameInvite.all_of(:invited_id => id, :invite_status => GameInvite::INVITE_STATUS[:pending])
  end

  def sent_invites
    GameInvite.all_of(:inviter_id => id, :invite_status => GameInvite::INVITE_STATUS[:pending])
  end
  
  # Cumulative results
  def wins
    wins_as_white + wins_as_black
  end
  
  def losses
    losses_as_white + losses_as_black
  end
  
  def draws
    draws_as_white + draws_as_black
  end

  def generate_token(column)
  	begin
  		self[column] = SecureRandom.urlsafe_base64
  	end while User.exists?(conditions: { column: self[column]})  	
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.now.utc
    save!
    UserMailer.password_reset(self).deliver
  end

  private

  def generate_verification_token
    generate_token(:verification_token)
  end

  def send_welcome_email
    UserMailer.welcome(self).deliver    
  end

end
