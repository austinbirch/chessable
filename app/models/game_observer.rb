class GameObserver < Mongoid::Observer
  #
  # When the game is updated, we want to alert the
  # opponent that it is their move next.
  #
  # If it is the first save, we want to let the oppenent know
  # that a game has been started with them.
  #
  def after_update(game)
    if game.next_turn == 'w'
      actor = game.black_player
      target_player = game.white_player
    else
      actor = game.white_player
      target_player = game.black_player
    end

    if game.game_over
      if game.in_checkmate
        notification = target_player.notifications.create(:actor => actor,
                                                          :message => 'won the game!',
                                                          :message_action => 'to view the result.',
                                                          :target_object => game)
        UserMailer.game_won_notification(target_player, notification).deliver if target_player.receive_email_notifications

        if game.white_player == actor
          actor.wins_as_white += 1
          target_player.losses_as_black += 1
          actor.save
          target_player.save
        else
          actor.wins_as_black += 1
          target_player.losses_as_white += 1
          actor.save
          target_player.save
        end

      elsif game.in_draw
        notification = target_player.notifications.create(:actor => actor,
                                                          :message => 'drew the game with you!',
                                                          :message_action => 'to view the result.',
                                                          :target_object => game)
        UserMailer.game_drew_notification(target_player, notification).deliver if target_player.receive_email_notifications

        if game.white_player == actor
          actor.draws_as_white += 1
          target_player.draws_as_black += 1
          actor.save
          target_player.save
        else
          actor.draws_as_black += 1
          target_player.draws_as_white += 1
          actor.save
          target_player.save
        end
      
      elsif !game.forfeit_player_id.nil?
        # we need to swap the actor and the target_player
        # as the next turn isn't switched due to no move being
        # played
        actor, target_player = target_player, actor
        
        notification = target_player.notifications.create(:actor => actor,
                                                          :message => 'forfeited the game!',
                                                          :message_action => 'to view the result.',
                                                          :target_object => game)
        UserMailer.game_forfeit_notification(target_player, notification).deliver if target_player.receive_email_notifications
        
        if game.white_player == actor
          actor.losses_as_white += 1
          target_player.wins_as_black += 1
          actor.save
          target_player.save
        else
          actor.losses_as_black += 1
          target_player.wins_as_white += 1
          actor.save
          target_player.save
        end        
      end

    else
      notification = target_player.notifications.create(:actor => actor,
                                                        :message => 'made a move!',
                                                        :message_action => 'to respond.',
                                                        :target_object => game)

      UserMailer.move_made_notification(target_player, notification).deliver if target_player.receive_email_notifications
    end
    
    actor.notifications.where(:target_object_id => game.id).each do |notification|
      notification.read = true
      notification.save!
    end

  end

end
