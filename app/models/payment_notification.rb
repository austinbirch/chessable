class PaymentNotification
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :user
  after_create :act_on_notification

  attr_accessible :params, :status, :transaction_type, :transaction_id, :user_id

  field :params, :type => Hash
  field :status
  field :transaction_type
  field :transaction_id

  private

  def act_on_notification
    if acknowledge?
      Rails.logger.info 'PAYPAL: Authorised notification receivced'

      case transaction_type
      when 'subscr_signup'
        #
        # Sent only when the user first subscribes to a paid plan
        #
        # There is potential for the user to have a subscription already,
        # as they may have been on a free plan before subscribing to the
        # paid plan.
        #
        if params[:mc_currency] == 'GBP' && params[:mc_amount3] == '3.00' &&
          params[:receiver_email] == APP_CONFIG[:paypal_email]

          # make sure there are not any other active subscriptions
          user.subscriptions.each do |subscr|
            subscr.current = false
            subscr.save
          end

          subscription = user.subscriptions.build
          original_tz = Time.zone
          Time.zone = ('Pacific Time (US & Canada)')
          subscription.end_date = Time.zone.parse(params[:subscr_date]).utc + 1.month
          subscription.start_date = Time.zone.parse(params[:subscr_date]).utc
          subscription.current = true
          subscription.save

          Time.zone = original_tz

          UserMailer.subscribed_to_plus(user).deliver
          Rails.logger.info "SUBSCRIPTION: Created subscription for #{user.inspect}"
        end

      when 'subscr_payment'
        #
        # Sent whenever the user makes a payment. We should get these
        # repeatedly as a user keeps a paying a subscription. We should have
        # a subscription to mark as paid, as receiving the subscr_signup will
        # have set up a new subscription marked as unpaid.
        #
        #
        if params[:mc_currency] == 'GBP' && params[:mc_gross] == '3.00' &&
          params[:receiver_email] == APP_CONFIG[:paypal_email]

          # 
          # check to make sure it was actually paid
          #
          if params[:payment_status] == 'Completed'
            if user.current_subscription
              sub = user.current_subscription
              sub.paid = true
              sub.save!
            else
              Rails.logger.info "SUBSCRIPTION: Could not find subscription to mark as paid for #{user.inspect}"
            end

            UserMailer.payment_received(user).deliver
            Rails.logger.info "SUBSCRIPTION: Marked subscription as paid for #{user.inspect}."
          else
            #
            # The payment failed, so we should send an email to the user to let
            # them know that there may be a problem with their account, and
            # that we'll try and take out payment in a few days
            #
            UserMailer.payment_failed(user).deliver
            Rails.logger.info "SUBSCRIPTION: Received subscr_payment, but the payment status was not 'Completed' for #{user.inspect}."
          end
        end

      when 'subscr_eot'
        #
        # Sent whenever the user finishes a paid subscription. i.e the last day
        # they paid up until.
        #
        # For example: If the user is on a monthly subscription (and doesn't
        # cancel), we should receive one of these a month
        #
        # We use this message to create a new subscription, and we should
        # receive a subscr_payment pretty soon after this.
        #
        if params[:mc_currency] == 'GBP' && params[:receiver_email] == APP_CONFIG[:paypal_email]
          user.subscriptions.each do |sub|
            sub.current = false
            sub.save
          end

          subscription = user.subscriptions.build
          original_tz = Time.zone
          Time.zone = ('Pacific Time (US & Canada)')
          subscription.end_date = Time.zone.parse(params[:subscr_date]).utc + 1.month
          subscription.start_date = Time.zone.parse(params[:subscr_date]).utc
          subscription.current = true
          subscription.save

          Time.zone = original_tz
        end        

      when 'subscr_cancel'
        #
        # Sent whenever somebody cancels their subscription through paypal. We
        # need to remove their active subscription, so that we know they are
        # not paying anymore.
        #
        # They will be prompted to purchase a free plan when their current
        # subscription reaches subscr_eot
        #
        if params[:mc_currency] == 'GBP' && params[:mc_gross] == '3.00' &&
          params[:receiver_email] == APP_CONFIG[:paypal_email]
          if user.current_subscription
            sub = user.current_subscription
            sub.cancelled = true
            sub.save!
          end

          UserMailer.subscription_cancelled(user).deliver!
          Rails.logger.info "SUBSCRIPTION: Cancelled subscription for #{user.inspect}."
        end

      end

    else
      Rails.logger.info 'PAYPAL: Unauthorised notification received'
    end

  end

  def acknowledge?
    params[:secret] == APP_CONFIG[:paypal_secret]
  end

end
