require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
	
  setup do
  	@user = FactoryGirl.create(:user)
  end

  test 'password_reset' do
  	mail = UserMailer.password_reset(@user)
  	assert_equal 'Password reset instructions', mail.subject
  	assert_equal [@user.email], mail.to
  	assert_equal ['hello@chessable.co.uk'], mail.from
  end

end
