require 'test_helper'

class GameInvitesControllerTest < ActionController::TestCase

	setup do
		@invited = FactoryGirl.create(:user)
		@inviter = FactoryGirl.create(:user)
		@game_invite = FactoryGirl.build(:game_invite, :invited => @invited, :inviter => @inviter)
	end

	test 'should be able to get show view for an invite' do
		@game_invite.save!
		sign_in @inviter
		get :show, :id => @game_invite

		assert_response :success
	end

	test 'should not be able to show game invite if you are not inviter or invited' do
		@game_invite.save!
		@user_two = FactoryGirl.create(:user)
		sign_in @user_two
		get :show, :id => @game_invite

		assert_redirected_to forbidden_path
	end

  test 'should be able to delete a game invite if you are the inviter' do
    @game_invite.save!
    invite_count = GameInvite.count

    sign_in @inviter
    delete :destroy, :id => @game_invite

    assert_equal GameInvite.count, (invite_count - 1)
    assert_redirected_to user_dashboard_path(@inviter)
  end

  test 'should not be able to delete a game invite if you are not the inviter' do
    @game_invite.save!
    invite_count = GameInvite.count

    sign_in @invited
    delete :destroy, :id => @game_invite

    assert_equal GameInvite.count, invite_count
    assert_redirected_to forbidden_path
  end

	test 'should be able to create a new game invite' do
		request.env["HTTP_REFERER"] = user_dashboard_path(@user)
		sign_in @inviter
		post :create, :game_invite => { :invited => @invited }

		assert_redirected_to user_dashboard_path
		assert_not_nil assigns[:game_invite]
	end

	test 'should be redirected to new_sessions_path if you are not signed in and try to create a game invite' do
		request.env["HTTP_REFERER"] = user_dashboard_path(@user)
		post :create, :game_invite => { :invited => @invited }

		assert_redirected_to new_session_path
		assert_nil assigns[:game_invite]
	end

	test 'should not be able to invite yourself to a game' do
		request.env["HTTP_REFERER"] = user_dashboard_path(@user)
		@inviter = @invited
		sign_in @inviter
		post :create, :game_invite => { :invited => @invited }

		assert_redirected_to user_dashboard_path
		assert_equal 'Something went wrong...', flash[:alert]
	end

	test 'should be able to accept a game invite if you are the invited player' do
		sign_in @invited
		@game_invite.save!

		put :update, :id => @game_invite, :game_invite => { :invite_status => GameInvite::INVITE_STATUS[:accepted] }
		assert_redirected_to game_path(assigns[:game_invite].game)
	end

	test 'should not be able to accept a game invite if you are not the invited player' do
		sign_in @inviter
		@game_invite.save!

		put :update, :id => @game_invite, :game_invite => { :invite_status => GameInvite::INVITE_STATUS[:accepted] }
		assert_redirected_to forbidden_path
	end

end
