require 'test_helper'

class GamesControllerTest < ActionController::TestCase

  setup do
    @user = FactoryGirl.create(:user)
    sign_in @user
    @game = FactoryGirl.create(:game)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test 'should be able to create a game' do
    request.env["HTTP_REFERER"] = 'http://localhost:3000/'
    @user_two = FactoryGirl.create(:user)

    post :create, :game => { :black_player => @user_two }
    assert_redirected_to game_path(assigns[:game].id)
  end

  test "should have the correct game on show action" do
    get :show, :id => @game
    assert_response :success
    assert_equal @game, assigns[:game]
  end

  test 'should have a list of all inactive and active games on index' do
    game_array = []
    game_array << @game
    get :index
    assert_equal [], assigns[:inactive_games].to_a
    assert_equal game_array, assigns[:active_games].to_a
  end
  

end
