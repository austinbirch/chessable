require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  
  test 'should respond to index' do
    get :index
    assert_response :success
  end
  
  test 'should respond to terms' do
    get :terms
    assert_response :success
  end
  
  test 'should respond to privacy' do
    get :privacy
    assert_response :success
  end
  
  test 'should respond to forbidden' do
    get :forbidden
    assert_response :success
  end
  
  test 'should respond to more_info' do
    get :more_info
    assert_response :success
  end
  
end
