require 'test_helper'

class SubscriptionsControllerTest < ActionController::TestCase

  setup do
    @user = FactoryGirl.create(:user)
  end
  
  test 'should be able to get new' do
    sign_in @user
    get :new, :user_id => @user

    assert_response :success
  end

  test 'the user instance variable should be set for the view' do
    sign_in @user
    get :new, :user_id => @user

    assert_not_nil assigns[:user]
  end

  test 'the subscriptions instance variable should be set for the view' do
    sign_in @user
    get :new, :user_id => @user

    assert_not_nil assigns[:subscription]
  end

  test 'should redirect to sign in if no current_user' do
    get :new, :user_id => @user

    assert_redirected_to new_session_path
  end

  test 'should redirect to forbidden if user tries to set another users subscription' do
    @user_two = FactoryGirl.create(:user)
    sign_in @user_two
    get :new, :user_id => @user

    assert_redirected_to forbidden_path
  end
  

end
