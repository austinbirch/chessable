require 'test_helper'

class VerificationsControllerTest < ActionController::TestCase

	setup do
		@user = FactoryGirl.create(:user, :verified => false)
	end

	test 'should verifiy an account if the correct token is supplied' do
		get :index, :verification_token => @user.verification_token

		assert_redirected_to new_user_subscription_path(@user)
		assert assigns[:user].verified
	end

  test 'should not verifiy if the verification token is incorrect' do
    get :index, :verification_token => 'hello'

    assert_redirected_to root_path
  end

  test 'should not allow a verification to occur if the account is already verified' do
    @user.verified = true
    @user.save!

    get :index, :verification_token => @user.verification_token
    assert_redirected_to root_path
  end

end
