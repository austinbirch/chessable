require 'test_helper'

class DashboardsControllerTest < ActionController::TestCase

	setup do
		@user = FactoryGirl.create(:user)
		sign_in @user
	end

  test 'should get index if logged in' do
    get :index
    assert_response :success
  end

  test 'should get redirected if not logged in and requesting index' do

  	cookies.delete(:auth_token)

  	get :index
  	assert_redirected_to new_session_path
  end

end
