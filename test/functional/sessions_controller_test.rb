require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  
  setup do
  	@user = FactoryGirl.build(:user)
  end

  test 'should redirect to dashboard action on sign in' do
  	@user.save!
  	post :create, :email => @user.email, :password => @user.password
  	assert_redirected_to user_dashboard_path(@user)
  end

  test 'should not allow an unverified user to sign in' do
  	@user.verified = false
  	@user.save!
  	post :create, :email => @user.email, :password => @user.password
  	assert_template :new
  end

end
