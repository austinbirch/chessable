require 'test_helper'

class CommentsControllerTest < ActionController::TestCase

  setup do
    @user = FactoryGirl.create(:user)
    sign_in @user

    @game = FactoryGirl.create(:game, :white_player => @user)
  end

  test 'user should be able to create a comment for a game' do
    comment_count = Comment.count
    post :create, :comment => { :user_id => @user.id, :content => 'This is a test' }, :game_id => @game.id

    assert_equal (comment_count + 1), Comment.count
    assert_redirected_to game_path(@game)
  end

  test 'user should not be able to create a comment if they are not a player in the game' do
    @user_two = FactoryGirl.create(:user)
    sign_out @user
    sign_in @user_two

    comment_count = Comment.count
    post :create, :comment => { :user_id => @user_two.id, :content => 'This is a test' }, :game_id => @game.id
    
    assert_equal comment_count, Comment.count
    assert_redirected_to forbidden_path
  end

  test 'user should not be able to create a comment if they are not signed in' do
    sign_out @user    

    comment_count = Comment.count
    post :create, :comment => { :user_id => @user.id, :content => 'This is a test' }, :game_id => @game.id

    assert_equal comment_count, Comment.count
    assert_redirected_to new_session_path
  end

end
