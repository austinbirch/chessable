require 'test_helper'

class UsersControllerTest < ActionController::TestCase
	
  setup do
    @user = FactoryGirl.create(:user)
    sign_in @user
  end

  test 'should get new action' do
    get :new
    assert_response :success
  end

  test 'should get index action' do
    get :index
    assert_response :success
  end

  test 'should get show action' do
    get :show, :id => @user
    assert_response :success
  end

  test 'should get edit action' do
    get :edit, :id => @user
    assert_response :success
  end
  
  test 'should be able to search for a username on users#index action' do
    users_array = []
    users_array << FactoryGirl.create(:user, :username => 'austinbirch')
    (0..20).each do
      FactoryGirl.create(:user)
    end
    get :index, :search => 'austinbirch'
    
    assert_equal users_array, assigns[:users]
  end

  test 'should be able to select a random user' do
    3.times do
      @user = FactoryGirl.create(:user)
    end
    get :random

    assert_response :redirect
  end

  #
  # tests for sorting on the users index action
  #
  # test 'should be able to sort users by name for index view in asc' do
  #   (0..20).each do 
  #     FactoryGirl.create(:user)
  #   end
  #   get :index, :sort_by => 'username'
  #   
  #   assert_equal -1, (assigns[:users].first.username <=> assigns[:users].last.username)
  # end
  # 
  # test 'should be able to sort users by win count for index view desc' do
  #   (0..20).each do
  #     FactoryGirl.create(:user)
  #   end
  #   get :index, :sort_by => 'most_wins', :direction => 'desc'
  #   
  #   assert_equal 1, (assigns[:users].first.username <=> assigns[:users].last.username)
  # end
  # 
  # test 'should be able to sort users by win count for index in asc' do
  #   (0..20).each do |i|
  #     FactoryGirl.create(:user, :wins => )
  #   end
  #   get :index, :sort_by => 'most_wins', :direction => 'asc'
  #   
  #   assert_equal 'hello', assigns[:users].last.wins
  #   assert assigns[:users].first.wins < assigns[:users].last.wins
  # end
  # 
  # test 'should be able to sort users by win count for index in asc' do
  #   (0..20).each do
  #     FactoryGirl.create(:user)
  #   end
  #   get :index, :sort_by => 'most_wins', :direction => 'asc'
  #   
  #   assert assigns[:users].first.wins < assigns[:users].last.wins
  # end
  #
  #
  # test 'should be able to sort users by loss count for index in desc' do
  #     (0..20).each do
  #       FactoryGirl.create(:user)
  #     end
  #     get :index, :sort_by => 'most_losses', :direction => 'desc'
  #     
  #     assert assigns[:users].first.losses > assigns[:users].last.losses
  #   end
  #   
  # test 'should be able to sort users by loss count for index in asc' do
  #   (0..20).each do
  #     FactoryGirl.create(:user)
  #   end
  #   get :index, :sort_by => 'most_losses', :direction => 'asc'
  #   
  #   assert assigns[:users].first.losses < assigns[:users].last.losses
  # end
  # 
  # test 'should be able to sort users by draw count for index in asc' do
  #   (0..20).each do
  #     FactoryGirl.create(:user)
  #   end
  #   get :index, :sort_by => 'most_draws', :direction => 'asc'
  #   
  #   assert assings[:users].first.draws < assings[:users].last.draws
  # end
  # 
  # test 'should be able to sort users by draw count for index in desc' do
  #   (0..20).each do
  #     FactoryGirl.create(:user)
  #   end
  #   get :index, :sort_by => 'most_draws', :direction => 'desc'
  #   
  #   assert assings[:users].first.draws > assings[:users].last.draws
  # end
  # 
  # test 'should be able to sort users by recent activity in asc' do
  #   (0..2).each do
  #     FactoryGirl.create(:user)
  #   end
  #   get :index, :sort_by => 'recent_activity'
  #   
  #   assert assigns[:users].first.updated_at < assigns[:users].last.updated_at
  # end
  
  

  # test 'should be able to create a user' do
  # 	@user = FactoryGirl.build(:user)
  # 	attrs = FactoryGirl.attributes_for(:user)
  # 	assert_equal 'hello', @user.attributes
  # 	post :create, :user => attrs
  # 	assert_redirected_to user_dashboard_path(@user)
  # 	assert_equal @user.email, assigns[:user].email
  # end

  test 'should redirect to the home page on creation of a user' do
    attrs = FactoryGirl.attributes_for(:user, :verified => false)
    post :create, :user => attrs

    assert_equal attrs[:email], assigns[:user].email
    assert_redirected_to root_path
  end

end
