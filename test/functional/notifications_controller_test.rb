require 'test_helper'

class NotificationsControllerTest < ActionController::TestCase

  setup do
    @user = FactoryGirl.create(:user)
    @notification = @user.notifications.create(FactoryGirl.attributes_for(:notification))
  end

  test 'should be able to update the read status of a notification' do
    sign_in @user
    request.env["HTTP_REFERER"] = user_dashboard_path(@user)
    put :update, :user_id => @notification.user, :id => @notification, :notification => { :read => true }

    assert_equal true, assigns[:notification].read
    assert_redirected_to user_dashboard_path
  end

  test 'should not be able to update the read status of another users notifications' do
    # log somebody else in
    @user_two = FactoryGirl.create(:user)
    sign_in @user_two

    # try and update another users notification read status
    request.env["HTTP_REFERER"] = user_dashboard_path(@user)
    put :update, :user_id => @notification.user, :id => @notification, :notification => { :read => true }

    assert_redirected_to forbidden_path
  end

end
