ENV["RAILS_ENV"] = "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  # fixtures :all

  # Add more helper methods to be used by all tests here...

  # Mongoid is not transactional, so we need to 
  # reset the database ourselves...
  # (http://www.allenwei.cn/clean-mongodb-in-unittest-rspec-cucumber-using-mongoid/)
  teardown :clean_mongodb
  def clean_mongodb
    Mongoid.database.collections.each do |collection|
      unless collection.name =~ /^system\./
        collection.remove
      end
    end
  end

  # Signs in the passed user
  def sign_in(user)
    if user.authenticate(user.password)
      cookies[:auth_token] = user.auth_token
    end
  end

  # Signs out the user
  def sign_out(user)
    cookies.delete(:auth_token)
  end

end
