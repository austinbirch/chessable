Factory.sequence :email do |n|
	"test#{n}@example.com"
end

Factory.sequence :username do |n|
	"username#{n}"
end

FactoryGirl.define do

	factory :user do
		username { Factory.next(:username) }
		email { Factory.next(:email) }
		password 'securepass'
		password_confirmation 'securepass'
    receive_email_notifications true
		verified true
	end

	factory :game do
		association :white_player, :factory => :user
		association :black_player, :factory => :user
	end

	factory :notification do
		association :user, :factory => :user
		association :target_object, :factory => :game
		association :actor, :factory => :user
		message 'made a move!'
		message_action 'to respond.'
	end

	factory :game_invite do
		association :invited, :factory => :user
		association :inviter, :factory => :user
	end
  
  factory :subscription do
  	association :user, :factory => :user
  	start_date Time.now.utc
  	end_date Time.now.utc + 1.month
  	paid false
  	current true
  	cancelled false
  end

  # factory :payment_notification do
  #   association :user, :factory => :user
  # end
  
  factory :game_comment, :class => 'Comment' do
    association :commentable, :factory => :game
    commentable_type 'Game'
    content 'This is a comment.'
  end

end
