require 'test_helper'

class SubscriptionTest < ActiveSupport::TestCase

	setup do
		@user = FactoryGirl.create(:user)
		@subscription = FactoryGirl.create(:subscription, :user => @user)
	end

	test 'the factory for a subscription should be valid' do
		assert @subscription.valid?
	end

  test 'should be able to generate an encrypted string for paypal' do
    assert @subscription.encrypted_paypal(Rails.application.routes.url_helpers.root_url(:host => 'localhost:3000'),
     Rails.application.routes.url_helpers.root_url(:host => 'localhost:3000'))
  end

end
