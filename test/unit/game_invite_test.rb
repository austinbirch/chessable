require 'test_helper'

class GameInviteTest < ActiveSupport::TestCase

	setup do
		@game_invite = FactoryGirl.build(:game_invite)
	end

	test 'game invite factory should be valid' do
		assert @game_invite.valid?
	end

	test 'game invite should not be valid without a inviter' do
		@game_invite.inviter = nil
		assert !@game_invite.valid?
	end

	test 'game invite should not be valid without an invited player' do
		@game_invite.invited = nil
		assert !@game_invite.valid?
	end

	test 'if game invite is rejected it should be destroyed' do
		@game_invite.save!
		game_invite_count = GameInvite.all.to_a.count
		@game_invite.invite_status = GameInvite::INVITE_STATUS[:rejected]
		@game_invite.save!
		
		assert_equal (game_invite_count - 1), GameInvite.all.to_a.count
	end

  test 'if game invite is accepted then a game should be created' do
    @game_invite.save!
    game_count = Game.all.count
    @game_invite.invite_status = GameInvite::INVITE_STATUS[:accepted]
    @game_invite.save!

    assert_equal (game_count + 1), Game.all.count
  end

  test 'if game invite is accepted when user active games >= 2 and no subscription it should not update' do
    @game_invite.save!

    @game_invite.invited.subscriptions.each do |sub|
      sub.destroy
    end
    2.times do
      FactoryGirl.create(:game, :white_player => @game_invite.inviter, :black_player => @game_invite.invited)
    end

    @game_invite.invite_status == GameInvite::INVITE_STATUS[:accepted]

    assert !@game_invite.update_attributes(:invite_status => GameInvite::INVITE_STATUS[:accepted])
  end

  test 'if game invite is accepted when inviter now has more than one active game and no subscription, it should fail updating' do
    @user = FactoryGirl.create(:user)
    @user.subscriptions.each { |sub| sub.destroy }

    2.times do
      FactoryGirl.create(:game, :white_player => @user)
    end

    @game_invite_two = FactoryGirl.build(:game_invite, :inviter => @user)

    assert !@game_invite_two.valid?
  end

end
