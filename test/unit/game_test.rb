require 'test_helper'

class GameTest < ActiveSupport::TestCase

  setup do
    @game = FactoryGirl.build(:game)
    @pgn = '1. e4 g5 2. Nf3 Bh6 3. h4 Nf6 4. hxg5 Bxg5 5. Nxg5 h6 6. Nf3 c5 7. b3 Nc6 8. Bb2 e5 9. Nxe5 Rf8 10. Nxc6 bxc6 11. e5 d6 12. exf6 Qe7+ 13. fxe7 Kxe7 14. Rxh6 f6 15. Qe2+ Kd7 16. Rxf6 Rxf6 17. Bxf6 Kc7 18. Nc3 c4 19. g3 Bd7 20. Qe7 Kc8 21. Bd4 Rb8 22. Qxd6 Rb6 23. Na4 Rxb3 24. Bxa7 Be8 25. Bh3+ Kb7 26. Qe7+ Ka8 27. Nb6+ Rxb6 28. Bxb6 Bg6'
  end

  test 'the game factory should be valid' do
    assert @game.valid?
  end

  test 'should be invalid without a white player' do
    @game.white_player = nil
    assert !@game.valid?
  end

  test 'should be invalid without a black player' do
    @game.black_player = nil
    assert !@game.valid?
  end

  # friendly_name of a game is simply Whiteplayer-vs-Blackplayer usernames
  test 'friendly_name should return the friendly name of a game' do
    friendly_string = "#{@game.white_player.username}-vs-#{@game.black_player.username}"
    assert_equal friendly_string, @game.friendly_name
  end

  test 'after creating a game it should set the default pgn' do
    @game.save!
    default_pgn = "[White \"#{@game.white_player.to_param}\"]\n[Black \"#{@game.black_player.to_param}\"]\n"
    assert_equal default_pgn, @game.pgn
  end

  test 'after updating the next turn should be switched to the other player' do
    # create it first
    @game.save!
    assert_equal 'w', @game.next_turn
    @game.last_move = 'e4'
    # this causes an update
    @game.save!
    assert_equal 'b', @game.next_turn
  end

  test 'when updating a game the opponent should receive a notification' do
    # create it first
    @game.save!

    # white to move
    assert_equal 0, @game.black_player.notifications.count
    @game.last_move = 'e4'
    @game.save!

    # black should have notifications
    assert_equal 1, @game.black_player.notifications.count
  end

  test 'should set notifications regarding this game to read on update' do
    # create it first
    @game.save!

    # white to move
    @game.last_move = 'e4'
    @game.save!

    assert_equal 1, @game.black_player.notifications.count

    # black to move
    @game.last_move = 'e5'
    @game.save!

    assert_equal 0, @game.black_player.notifications.where(:read => false).count
  end
  
  test 'should be able to forfeit a game' do
    @game.save!

    @white = @game.white_player
    @game.pgn = @pgn
    @game.save!

    assert_equal 'w', @game.next_turn
    
    @game.forfeit_player_id = @game.white_player.id
    @game.save!
    
    assert @game.game_over
    assert_equal @game.black_player, @game.winner
  end

  #
  # FIXME:This test is not working for some reason, but it works in development???
  #
  # test 'the game should know when a move causes the game to be in check' do
  #     @game.pgn = @pgn
  #     Rails.logger.info "VALIDATOR:(TEST_FILE) #{@game.pgn}"
  #     @game.save!
  # 
  #     assert_equal false, @game.in_check
  # 
  #     # move the queen into a check position
  #     @game.last_move = 'Qe8+'
  #     @game.save!
  # 
  #     assert_equal true, @game.in_check
  #   end

  #
  # FIXME: This test is not working for some reason, but it works in development???
  #
  # test 'the game should know when a move causes the game to be in checkmate' do
  #     @game.pgn = @pgn
  #     @game.save!
  # 
  #     assert_equal false, @game.in_checkmate
  # 
  #     # move the queen into a checkmate position
  #     @game.last_move = 'Qa7#'
  #     @game.save! 
  # 
  #     assert_equal true, @game.in_checkmate
  #   end
  
  test 'the game should set winner to the white player if they won' do
    @game.save!

    @white = @game.white_player
    @game.pgn = @pgn
    @game.save!

    assert_equal 'w', @game.next_turn

    # move the queen into a checkmate position
    @game.last_move = 'Qa7#'
    @game.save!

    assert_equal @white, @game.winner
  end

  # test 'the game should set a draw flag if the game was a draw' do
  #   @game.pgn = '1. d4 f5 2. e3 Nf6 3. Nf3 e6 4. Bd3 Be7 
  #   5. Nbd2 d5 6. c4 c6 7. Qa4 O-O 8. Qc2 Qe8 9. b3 Nbd7 
  #   10. Bb2 Qg6 11. Rg1 Bb4 12. O-O-O Qe8 13. h3 Qe7 
  #   14. g4 Ne4 15. Nxe4 dxe4 16. Be2 exf3 17. Bxf3 fxg4 
  #   18. Bxg4 Nf6 19. Be2 Ba3 20. e4 Bxb2+ 21. Kxb2 Bd7 
  #   22. Rg3 Rac8 23. Rdg1 Rf7 24. e5 Ne8 25. Bd3 g6 
  #   26. Rg4 Qf8 27. R1g2 Rc7 28. h4 Rg7 29. h5 gxh5 
  #   30. Bxh7+ Kh8 31. Rxg7 Nxg7 32. Bg6 Be8 33. Be4 Nf5 
  #   34. Qd3 Rg7 35. Rh2 Bg6 36. Qd1 Qd8 37. d5 cxd5 38. cxd5 Qb6 
  #   39. Kc3 Qc5+ 40. Kb2 Qd4+ 41. Qxd4 Nxd4 42. Bxg6 Rxg6 
  #   43. Rxh5+ Kg7 44. d6 Nc6 45. d7 Rg2 46. Rh3 Rxf2+ 47. Ka3 Rf7 
  #   48. Rd3 Rf8 49. b4 b5 50. Rg3+ Kf7 51. Rf3+ Ke7 52. Rxf8 Kxf8 
  #   53. Kb3 Ke7 54. a4 a6 55. a5 Nxe5 56. Kc3 Kxd7 57. Kd4 Nc6+ 58. Kc5'

  #   @game.next_turn = 'b'
  #   assert_equal 'b', @game.next_turn
  #   @game.last_move = 'Kc7'
  #   @game.save!

  #   assert_equal 'hello', @game.in_draw
  #   assert @game.in_draw
  # end
  
  test 'the winners win count shoud rise by one when they win' do
    @game.save!

    @white = @game.white_player
    @game.pgn = @pgn
    @game.save!

    assert_equal 'w', @game.next_turn

    # move the queen into a checkmate position
    @game.last_move = 'Qa7#'
    @game.save!

    assert_equal 1, @white.wins_as_white
    assert_equal 1, @game.black_player.losses_as_black
  end
  
  test 'should be able to return the next player to move' do
    @game.save!
    
    white_player = @game.white_player
    
    assert_equal 'w', @game.next_turn
    
    assert_equal white_player, @game.next_player
  end


end
