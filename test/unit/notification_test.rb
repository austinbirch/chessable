require 'test_helper'

class NotificationTest < ActiveSupport::TestCase

  setup do
    @notification = FactoryGirl.build(:notification)
  end

  test 'notification factory should be valid' do
    assert @notification.valid?
  end

  test 'read status should be set to false by default' do
    assert_equal false, @notification.read
  end
  
  test 'once a notitification is read it should be destroyed' do
    @notification.save
    note_count = Notification.all.to_a.count
    
    @notification.read = true
    @notification.save!
    
    assert_equal (note_count - 1), Notification.all.to_a.count
  end

end
