require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  setup do
    @user = FactoryGirl.build(:user)
    @user_two = FactoryGirl.build(:user)
  end

  test 'user factory should be valid' do
    assert @user.valid?
  end

  test 'user_two factory should be valid' do
    assert @user_two.valid?
  end

  test 'should invalid without an email' do
    @user.email = nil
    assert !@user.valid?
  end

  test 'should not be invalid with an improper email' do
    @user.email = 'hello'
    assert !@user.valid?
  end

  test 'should be invalid with non-unique email' do
    @user.save!
    @user_two.email = @user.email
    assert !@user_two.valid?
  end

  test 'should be invalid wihtout a username' do
    @user.username = nil
    assert !@user.valid?
  end

  test 'should be invalid with a username longer than 24 chars' do
    @user.username = '123456789012345678901234567890'
    assert !@user.valid?
  end

  test 'should be invalid with a username less than two chars long' do
  @user.username = 'a'
  assert !@user.valid?
  end

  test 'should be invalid with a non-unique username' do
    @user.save!
    @user_two.username = @user.username
    assert !@user_two.valid?
  end

  test 'should be invalid without a password' do
    @user.password = nil
    @user.password_confirmation = nil
    assert !@user.valid?
  end

  test 'should be invalid with a password less than eight chars long' do
    @user.password = 'less'
    @user.password_confirmation = 'less'
    assert !@user.valid?
  end

  test 'should be able to return a list of unread notifications' do
    @user.save!
    @user.notifications.create(FactoryGirl.attributes_for(:notification))
    assert_equal 1, @user.unread_notifications.count
  end

  test 'should be able to return a list of game invites' do
    @user.save!
    @game_invite = FactoryGirl.create(:game_invite, :invited => @user)
    assert_equal 1, @user.pending_game_invites.count
  end

  test 'should be able to return a list of game invites that the user has sent' do
    @user.save!
    @game_invite = FactoryGirl.create(:game_invite, :inviter => @user)
    assert_equal 1, @user.sent_invites.count
  end
  
  test 'should be able to return a list of games' do
    @game_one = FactoryGirl.create(:game, :white_player => @user)
    @game_two = FactoryGirl.create(:game, :white_player => @user, :updated_at => Time.now - 1.hour)
    game_array = []
    game_array << @game_one
    game_array << @game_two
    game_array.sort_by { |game| game.updated_at }.reverse!
    
    assert_equal game_array.first, @game_one
    assert_equal game_array.last, @game_two
  end

  test 'after creating a user it should generate a verification token' do
    @user.save!
    assert_not_nil @user.verification_token
  end

  test 'should be able to get the current subscription' do
    assert_respond_to @user, :current_subscription
  end
  
  test 'should be able to get the cumulative total of wins' do
    @user.wins_as_white = 10
    @user.wins_as_black = 20
    assert_equal 30, @user.wins
  end
  
  test 'should be able to get the cumulative total of losses' do
    @user.losses_as_white = 10
    @user.losses_as_black = 10
    assert_equal 20, @user.losses
  end
  
  test 'should be able to get the cumulative total of draws' do
    @user.draws_as_black = 100
    @user.draws_as_white = 50
    assert_equal 150, @user.draws
  end
  
  test 'should be able to get the games that are still active' do
    assert_respond_to @user, :active_games
  end
  
  test 'should be able to get the games that have been won' do
    assert_respond_to @user, :won_games
  end
  
  test 'should be able to get the games that have resulted in a draw' do
    assert_respond_to @user, :drew_games
  end

end
