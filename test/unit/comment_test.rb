require 'test_helper'

class CommentTest < ActiveSupport::TestCase

  setup do
    @comment = FactoryGirl.build(:game_comment)
  end

  test 'the game comment factory should be valid' do
    assert @comment.valid?
    assert @comment.save!
  end

  test 'a comment should not be valid with less than two characters of content' do
    @comment.content = 'A'
    assert !@comment.valid?
  end

  test 'a comment should not be valid without a content' do
    @comment.content = nil
    assert !@comment.valid?
  end

  test 'a comment should not be valid with a blank content' do
    @comment.content = ''
    assert !@comment.valid?
  end

  test 'a comment should not be saved with trailing spaces' do
    @comment.content = 'Hello '
    @comment.save

    assert_equal @comment.content, 'Hello'
  end

end
