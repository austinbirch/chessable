Chessable::Application.routes.draw do

  get "subscriptions/new"

  root :to => 'home#index'
  match '/forbidden' => 'home#forbidden', :as => :forbidden
  match '/terms' => 'home#terms', :as => :terms
  match '/privacy' => 'home#privacy', :as => :privacy
  match '/more-info' => 'home#more_info', :as => :more_info

  resources :users, :path => :players do
    match '/dashboard' => 'dashboards#index'
    
    collection do
      get 'random'
    end

    resources :notifications, :only => [:update]
    resources :subscriptions, :only => [:new]
  end

  resources :games, :except => [:new, :edit] do
    resources :comments, :only => [:create]
  end

  resources :game_invites, :only => [:create, :update, :show, :destroy]
  resources :sessions
  resources :password_resets
  resources :payment_notifications, :only => [:create]

  resources :verifications, :only => [:index]

end
