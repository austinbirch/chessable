namespace :chessable do

  desc "Regenerate FEN notation for all current games"
  task :regenerate_fens => :environment do
    puts "Regenerating FENs..."
    
    # when we save, we don't want any callbacks
    User.reset_callbacks(:save)
    User.reset_callbacks(:update)
    User.reset_callbacks(:create)
    User.reset_callbacks(:validate)
    
    Notification.reset_callbacks(:save)
    Notification.reset_callbacks(:update)
    Notification.reset_callbacks(:create)
    Notification.reset_callbacks(:validate)
    
    Game.reset_callbacks(:save)
    Game.reset_callbacks(:update)
    Game.reset_callbacks(:create)
    Game.reset_callbacks(:validate)
    
    PaymentNotification.reset_callbacks(:save)
    PaymentNotification.reset_callbacks(:update)
    PaymentNotification.reset_callbacks(:create)
    PaymentNotification.reset_callbacks(:validate)
    
    Subscription.reset_callbacks(:save)
    Subscription.reset_callbacks(:update)
    Subscription.reset_callbacks(:create)
    Subscription.reset_callbacks(:validate)
    
    GameInvite.reset_callbacks(:save)
    GameInvite.reset_callbacks(:update)
    GameInvite.reset_callbacks(:create)
    GameInvite.reset_callbacks(:validate)
    
    Game.all.each do |game|
      puts "Before set FEN: #{game.fen}"
      Rails.logger.info "Before set FEN: #{game.fen}"

      source = File.read(Rails.root.join('lib/chess_validator.js'))
      context = ExecJS.compile(source)  

      ret_hash = context.call('validate', game.pgn)
      game.fen = ret_hash['fen']

      game.save!
      puts "After save FEN: #{game.fen}"
      Rails.logger.info "After save FEN: #{game.fen}"
    end
    
  end

end
