namespace :chessable do
  
  desc "Convert users to new mongoid_slug setup rather than key field setup"
  task :convert_to_mongoid_slug => :environment do
    new_users = []
    
    # cache all users
    old_users = User.all.to_a
    
    # when we save, we don't want any callbacks
    User.reset_callbacks(:save)
    User.reset_callbacks(:update)
    User.reset_callbacks(:create)
    User.reset_callbacks(:validate)
    
    Notification.reset_callbacks(:save)
    Notification.reset_callbacks(:update)
    Notification.reset_callbacks(:create)
    Notification.reset_callbacks(:validate)
    
    Game.reset_callbacks(:save)
    Game.reset_callbacks(:update)
    Game.reset_callbacks(:create)
    Game.reset_callbacks(:validate)
    
    PaymentNotification.reset_callbacks(:save)
    PaymentNotification.reset_callbacks(:update)
    PaymentNotification.reset_callbacks(:create)
    PaymentNotification.reset_callbacks(:validate)
    
    Subscription.reset_callbacks(:save)
    Subscription.reset_callbacks(:update)
    Subscription.reset_callbacks(:create)
    Subscription.reset_callbacks(:validate)
    
    GameInvite.reset_callbacks(:save)
    GameInvite.reset_callbacks(:update)
    GameInvite.reset_callbacks(:create)
    GameInvite.reset_callbacks(:validate)
    
    
    # replicate users, but skip _id field
    User.all.each do |user|
      new_user = User.new
      user.attributes.each_pair do |k, v|
        unless k == '_id'
          new_user[k.to_sym] = v
        end
      end
      new_users << new_user
    end
    
    # destroy all users
    User.all.each { |user| user.destroy }
    
    # re-create all users from replication
    new_users.each { |user| user.save!(:validate => false) }
    
    # fix associations
    old_users.each do |old_user|
      new_user = User.where(:username => old_user[:username]).first
      
      Game.all.each do |game|
        if game.white_player_id == old_user.id
          game.white_player_id = new_user.id
          game.save!(:validate => false)
        end
        
        if game.black_player_id == old_user.id
          game.black_player_id = new_user.id
          game.save!(:validate => false)
        end
        
        if game.winner_id == old_user.id
          game.winner_id = new_user.id
          game.save!(:validate => false)
        end
        
        if game.forfeit_player_id == old_user.id
          game.forfeit_player_id = new_user.id
          game.save!(:validate => false)
        end
      end
      
      Notification.all.each do |note|
        if note.user_id == old_user.id
          note.user_id = new_user.id
          note.save!(:validate => false)
        end
        
        if note.actor_id == old_user.id
          note.actor_id = new_user.id
          note.save!(:validate => false)
        end
      end
      
      Subscription.all.each do |sub|
        if sub.user_id == old_user.id
          sub.user_id = new_user.id
          sub.save!(:validate => false)
        end
      end
      
      PaymentNotification.all.each do |payment|
        if payment.user_id == old_user.id
          payment.user_id = new_user.id
          payment.save!(:validate => false)
        end
      end
      
      GameInvite.all.each do |game_invite|
        if game_invite.inviter_id == old_user.id
          game_invite.inviter_id = new_user.id
          game_invite.save!(:validate => false)
        end
        
        if game_invite.invited_id == old_user.id
          game_invite.invited_id = new_user.id
          game_invite.save!(:validate => false)
        end      
      end
      
    end    
  end
  
end